#!/usr/bin/env python3

import sys
import pychrome

sys_argv = sys.argv

if len(sys_argv) < 2:
    print("first argument needs to be day|night")  
    exit(1)

command = sys_argv[1]

if command == "night":
    expression = js_dark_to_light = """
$("body").removeClass("android").addClass("android-dark");
"""
elif command == "day":
    expression = js_light_to_dark = """
$("body").removeClass("android-dark").addClass("android");
""" 
else:
    print("first argument needs to be day|night")
    exit(1)

browser = pychrome.Browser(url="http://127.0.0.1:9222")
tab = browser.list_tab()[0]
tab.start()

tab.call_method('Runtime.evaluate', expression=expression)

tab.stop()
