#!/bin/bash

if [ -z $1 ]; then
  echo "use day|night as first argument"
  exit 1;
fi


status=$1

if [ "$status" == "day" ]; then
  echo "day"
  bg_color="#ffffff"
  fg_color="#000000"
else
  echo "night"
  bg_color="#000000"
  fg_color="#ffffff"
fi

sed -i "s/background_color = \".*\"/background_color = \"${bg_color}\"/" ~/.config/terminator/config
sed -i "s/foreground_color = \".*\"/foreground_color = \"${fg_color}\"/" ~/.config/terminator/config
