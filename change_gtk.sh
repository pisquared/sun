#!/bin/bash

if [ -z $1 ]; then
  echo "use day|night as first argument"
  exit 1;
fi


status=$1

if [ "$status" == "day" ]; then
  echo "day"
  value=0
  theme_name="elementary"
else
  echo "night"
  value=1
  theme_name="elementary_dark"
fi

sed -i "s/gtk-application-prefer-dark-theme=.*/gtk-application-prefer-dark-theme=$value/" ~/.config/gtk-3.0/settings.ini
# sed -i "s/gtk-theme-name=.*/gtk-theme-name=$theme_name/" ~/.config/gtk-3.0/settings.ini
