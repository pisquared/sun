#!/bin/bash

if [ -z $1 ]; then
  echo "use day|night as first argument"
  exit 1;
fi


status=$1

if [ "$status" == "day" ]; then
  echo "day"
  value="classic"
else
  echo "night"
  value="oblivion"
fi

gsettings set org.mate.pluma color-scheme $value
