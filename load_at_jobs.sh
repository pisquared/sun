#!/bin/bash
jobs=`atq | wc -l`

if [ $jobs -ne 4 ]; then
  cd /home/pi2/workspace/sun
  sunrise=$(./current.sh | cut -d' ' -f2)
  sunset=$(./current.sh | cut -d' ' -f3)
  at -f term.sh "$sunrise"
  at -f term.sh "$sunset"
  at -f ff_new_page.sh "$sunrise"
  at -f ff_new_page.sh "$sunset"
fi
