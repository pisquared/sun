#!/bin/bash

if [ -z $1 ]; then
  echo "use day|night as first argument"
  exit 1
fi

status=$1
FF_SETTINGS_PATH=`find ~/.mozilla/firefox -name *.default`

if [ "$status" == "day" ]; then
  echo "day"
  bg_color="#ffffff"
else
  echo "night"
  bg_color="#222222"
fi

mkdir -p ${FF_SETTINGS_PATH}/chrome
cat > ${FF_SETTINGS_PATH}/chrome/userContent.css << EOF
@-moz-document url("about:newtab") {
  body { 
    background-color: $bg_color !important;
  }
}
EOF
