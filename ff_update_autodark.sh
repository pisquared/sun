FF_SETTINGS_PATH=`find ~/.mozilla/firefox -name *.default`

sunrise=`./current.sh | cut -d' ' -f2`
sunset=`./current.sh | cut -d' ' -f3`

echo "Setting sunrise/set times as $sunrise - $sunset"

sqlite3 ${FF_SETTINGS_PATH}/webappsstore.sqlite << EOF
update webappsstore2 set value="$sunrise" where key="tbthemesinfosunriseTime";
update webappsstore2 set value="$sunset" where key="tbthemesinfosunsetTime";
EOF
