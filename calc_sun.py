#!/usr/bin/env python3
import sys
import datetime
import argparse
import pytz

from astral import Astral, Location

parser = argparse.ArgumentParser(description='Calculate sunrise/sunset')
parser.add_argument('--loc', dest='location', help='provide location')
parser.add_argument('--lat', dest='latitude', help='provide longitude')
parser.add_argument('--lon', dest='longitude', help='provide longitude')
parser.add_argument('--tz', dest='timezone', default="UTC", help='provide timezone')
parser.add_argument('--date-format', dest='date_format', default="%H:%M", help='provide datetime format')

args = parser.parse_args()



if args.location:
    a = Astral()
    loc = a[args.location]
    now = datetime.datetime.now(pytz.timezone(loc.timezone))
elif args.latitude and args.longitude:
    now = datetime.datetime.now(pytz.timezone(args.timezone))
    loc = Location(('', '', float(args.latitude), float(args.longitude), args.timezone, 0))    
else:
    print("usage: calc --loc=location_name [--date-format] \n       calc --lat=latitude --lon=longitude [--tz|--date-format]")
    exit(1)

sun = loc.sun(date=now, local=True)

sunrise = sun['sunrise'] 
sunset = sun['sunset']
print("{now} {sunrise} {sunset} {status}".format(now=now.strftime(args.date_format),
                                                 sunrise=sunrise.strftime(args.date_format),
                                                 sunset=sunset.strftime(args.date_format),
                                                 status="day" if sunrise < now < sunset else "night"))

